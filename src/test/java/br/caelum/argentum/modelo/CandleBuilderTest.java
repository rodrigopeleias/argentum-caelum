package br.caelum.argentum.modelo;

import static org.junit.Assert.*;

import java.util.Calendar;

import org.junit.Test;

import br.com.caelum.argentum.modelo.CandleBuilder;

public class CandleBuilderTest {

	@Test
	public void  geracaoDeCandleDeveTerTodosOsDadosNecessariosTest() {
		new CandleBuilder().comAbertura(10)
			.comFechamento(20).comMinimo(20).comMaximo(50)
			.comVolume(10000).comData(Calendar.getInstance()).geraCandle();		
	}

}
