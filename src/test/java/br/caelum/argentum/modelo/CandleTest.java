package br.caelum.argentum.modelo;

import java.util.Calendar;

import org.junit.Test;

import br.com.caelum.argentum.modelo.Candle;

public class CandleTest {

	@Test(expected=IllegalArgumentException.class)
	public void precoMaximoNaoPodeSerMenorQueMinimoTest() {
		new Candle(10, 20, 20, 10, 10000, Calendar.getInstance());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void precoMaximoNaoPodeDataNulaTest() {
		new Candle(30, 20, 20, 40, 10000, null);
	}

}
