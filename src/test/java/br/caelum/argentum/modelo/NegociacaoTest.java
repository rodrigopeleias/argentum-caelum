package br.caelum.argentum.modelo;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

import br.com.caelum.argentum.modelo.Negociacao;

public class NegociacaoTest {

	@Test
	public void dataDeNegociacaoImutavelTest() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 15);
		Negociacao negociacao = new Negociacao(10, 5, c);
		
		negociacao.getData().set(Calendar.DAY_OF_MONTH, 20);
		assertEquals(15, negociacao.getData().get(Calendar.DAY_OF_MONTH));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void naoCriaNegociacaoComDataNula() {
		new Negociacao(10, 5, null);
	}
	
	@Test
	public void mesmoMilissegundoEhDoMesmoDiaTest() {
		Calendar agora = Calendar.getInstance();
		Calendar mesmoMomento = (Calendar)agora.clone();
		
		Negociacao negociacao = new Negociacao(40.0, 100, agora);
		assertTrue(negociacao.isMesmoDia(mesmoMomento));
	}
	
	@Test
	public void comHorariosDiferentesEhNoMesmoDiaTest() {
		Calendar manha = new GregorianCalendar(2014, 10, 20, 8, 30);
		Calendar tarde = new GregorianCalendar(2014, 10, 20, 15, 30);
		
		Negociacao negociacao = new Negociacao(40.0, 100, manha);
		assertTrue(negociacao.isMesmoDia(tarde));
	}
	
	@Test
	public void mesmoDiaMasMesesDiferentesNaoSaoDoMesmoDiaTest() {
		Calendar manha = new GregorianCalendar(2014, 11, 20, 8, 30);
		Calendar tarde = new GregorianCalendar(2014, 10, 20, 15, 30);
		
		Negociacao negociacao = new Negociacao(40.0, 100, manha);
		assertFalse(negociacao.isMesmoDia(tarde));
	}	
	
	@Test
	public void mesmoDiaEMesMasAnosDiferentesNaoSaoDoMesmoDiaTest() {
		Calendar manha = new GregorianCalendar(2016, 10, 20, 8, 30);
		Calendar tarde = new GregorianCalendar(2014, 10, 20, 15, 30);
		
		Negociacao negociacao = new Negociacao(40.0, 100, manha);
		assertFalse(negociacao.isMesmoDia(tarde));
	}	

}
